# CoinGecko ReactNative Engineering Written Assignment - Shopping Cart

Buying cryptocurrencies is as easy as shopping for a new pair shoes in 2022! 

You are inspired to build a simple **React Native** application called **GeckoCart** to browse and buy cryptocurencies using [CoinGecko.com](https://coingecko.com/)'s price data. 

You plan on using the CoinGecko [public API](https://www.coingecko.com/api/documentations/v3#/) for this project. 

## Software Specifications

Your application should provide the following user journeys: 

#### Browsing
- Users can see a list of coins displayed with their symbol, current price and 24 hr price change. 
- Users can view a single coin to view additional information about the coin including circulating supply, total supply, all-time highs and lows (ATH and ATL)
- Users can search for a coin or list of coins by their name or symbol

#### Buying
- Users can add a single coin to a shopping cart
- Users can specify the amount of the coin e.g. _0.05 BTC_ or _2 ETH_ when adding to cart. Users can also edit the amounts of items in cart.
- Users can see a running total in terms of USD/$ in their cart
- Users can quit the GeckoCart app and be able to resume their cart on reopening the app.
- Users can checkout or submit their cart for purchase. You can simply simulate a successful checkout flow for this part of the app.

#### Receipt
- Upon successful checkout, a user can view a history of orders in the application. 
- The history of orders should contain the purchase price of all items, and total paid amount.


### Scoring Guide

All submissions will be evaluated based on the following criteria: 

- Completeness of solution including documentation and deployment
- Clean, understandable and proper version-control practice
- A simple and neat UI and UX. The layout should also be responsive for portrait and landscape layouts. 


#### Extra Credit

**L3 and above candidates** will additionally be evaluated for the following:

- Test coverage and overall approach to automated testing including unit tests and integration tests
- Implementation of Redux for state management 
- Configuring CI/CD for deployment
- Error and edge-case handling beyond the given user happy path. 
- Additional security features including data encryption and device authentication methods e.g. PIN, Biometry.


## Submission Guide

- Fork this code into your Gitlab account as a **Private** repository and invite the users in https://gitlab.com/groups/coingecko-dev-test/-/group_members

- Your submission should include a **README** that includes at least an **installation guide, dependencies and other relevant information** (such as scaffolding, reused custom components) for the reviewing team. You are encouraged to reuse any existing components you have developed prior - make note of this in the README.

- We do not expect candidates to spend more than 2 weeks on the assignment. Your submission will be used as a **primary subject for the next/final round** of interview. Your submission should help illustrate your depth and breadth of experience corresponding to the job level expectations for your application, without being exhaustive.

- You may use our [stackshare.io](https://stackshare.io/coingecko/coingecko-mobile) profile as a point of reference.


